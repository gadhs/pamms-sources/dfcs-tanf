= 1011 Health Insurance Portability and Accountability Act (HIPAA) of 1996
:chapter-number: 1000
:effective-date: August 2019
:mt: MT-49
:policy-number: 1011
:policy-title: Health Insurance Portability and Accountability Act of 1996
:previous-policy-number: MT 41

include::partial$policy-header.adoc[]

== Requirements

The Georgia Department of Human Services (DHS) is required to comply with the Health Insurance Portability and Accountability Act (HIPAA) of 1996, including the Act's rules pertaining to the security and privacy of confidential health information.

== Basic Considerations

The U.S. Congress passed HIPAA in 1996 for the purpose of protecting health insurance coverage for workers when they change or lose their jobs.

HIPAA was designed to ensure patient confidentiality for all health care related information.
HIPAA's requirements apply to any entity storing and/or transmitting patient identifiable information on electronic media.

HIPAA was also designed to reduce health care costs by transferring administrative and financial transactions to electronic media.
HIPAA established standards for safeguarding the transmission and storage of private medical information.

A failure to comply with the mandates contained in the HIPAA can result in civil and criminal penalties.

=== Covered Entity

HIPAA rules define a covered entity as a (1) health plan, (2) health care clearinghouse, or (3) health care provider that electronically transmits health information.

Georgia DHS is a covered entity.

=== HIPAA Applicability

HIPAA rules apply to all DHS employees, volunteers, trainees, and contractors whose duties give them access to, or may involve the distribution, modification, and/or management of protected health information.

=== Protected Health Information

Protected Health Information (PHI) is health information that is identifiable as belonging to a individual.
Examples of PHI include, but are not limited to the following:

* demographic information, such as name, age, gender and social security number,
* health status information
* prescription drug information
* healthcare payment information
* prior existing conditions
* eligibility information
* authorization and referral certifications.

PHI may be transmitted electronically, via hard copy, or orally.

=== Privacy Rule

The Privacy Rule (PR), effective April 14, 2003, protects private health information by limiting the ways in which PHI can be used and released.

The Privacy Rule applies only to a covered entity, and not necessarily to every organization that uses, collects, accesses, and discloses individually identifiable health information.
An organization that does not meet the definition of a covered entity does not have to comply with the Privacy Rule.

=== DHS Confidentiality Requirements

DHS administers programs and provides services that have more stringent requirements than those established by the Privacy Rule.
DHS will adhere to its more stringent requirements in the administration of the programs and in the provision of the services for which DHS is responsible.

=== Notice of Privacy Practices

Every person in an AU, 18 years or older, must be provided with a Form 5460, Notice of Privacy Practices, upon receipt of an application for assistance.
The HIPAA notice must also be generated for Authorized Representatives, Protective Payees and/or minors who are head of household regardless of age.
Thereafter a new form will only be required if any change occurs in DHS's HIPAA policy.

The notice must be mailed to the client if a face-to-face interview is not completed or if AU members 18 years or older are not present for the TANF interview.
It is preferable, but not required, that the client sign and return the notice.
The case record must be documented if the notice is manually mailed.

=== Information Sharing

A covered entity may use and share only the minimum amount of protected information necessary to accomplish a particular purpose.

DHS is responsible for determining the amount of PHI required to accomplish a specific task.
Upon making this determination, DHS will communicate its decision to all affected parties.

=== PHI Use and Disclosure

The Privacy Rule prohibits the use and disclosure of PHI for purposes not related to treatment, payment, or health care operations.

Prior to release of PHI, the department must verify the identity of any person requesting PHI and that person's authority to receive such information.

As a covered entity, DHS is permitted, but not required, to use and disclose PHI without an individual's authorization only in certain situations and for specific purposes.

The following uses and disclosures do not require authorization from a client or the client's representative:

* treatment, payment, and health care operations
* public health agency activities
* health oversight and regulatory agency activities
* judicial proceedings and law enforcement investigations
* health care fraud investigations
* emergency situations
* health information not connected with other information that identifies the individual.

The following uses and disclosures of a client's PHI require authorization from the client or the client's representative:

* third party disclosures
* marketing and fundraising activities
* non-health related affiliates
* underwriting or risk-rating activities
* employment determinations
* sale, rental, or barter of PHI
* psychotherapy records other than psychotherapy notes.

=== Form 5459

Prior to the release of PHI that requires authorization from the client or the client's representative, the client or representative must complete and sign Form 5459, Authorization for Release of Information.

Form 5459 may be used to release or obtain information only if the client or representative specifies, on the form, to whom information is to be released or from whom information is to be obtained.

=== Administrative Requirements

DHS must comply with HIPAA Privacy Rule administrative requirements including, but not limited to the following:

* designation of a privacy officer who is responsible for the development, implementation, and maintenance of privacy policies and procedures
* development, implementation, and documentation of timely and effective privacy training
* development, maintenance, and enforcement of complaint procedures
* enforcement of appropriate sanctions for failure to comply with HIPAA regulations.

=== Security Rule

The HIPAA Security Rule ensures the security of PHI by specifying how PHI is to be stored, transmitted, and accessed.

=== Phi Safeguarding Practices

Guidelines for safeguarding PHI include, but are not limited to the following:

* PHI must only be discussed with the client or Privacy Rule in a private area.
* PHI must only be discussed with another staff member on a need-to-know basis and only in a non-public area.
* A telephone call conversation regarding PHI must only be held in an area where the conversation cannot be overheard.
* When conducting a client interview, the case manager must position the computer monitor in a way that does not permit observation by anyone other than the client or Privacy Rule.
* Computer passwords must not be shared and must be recorded only in a secure location.
* PHI must be disclosed only by staff authorized to do so.
* Access to fax machines must be limited to authorized staff.
* Case records, mail, documentation, and other materials containing PHI must be maintained in a locked or otherwise secure location, away from the general public.
* Staff must always wear appropriate agency-issued identification.
* PHI must be discarded in appropriate, secure containers.

=== Administrative Requirements

DHS must comply with HIPAA Security Rule administrative requirements including, but not limited to, the following:

* development and enforcement of information access control
* completion of internal security audits
* enforcement of physical safeguards including workstation/office guidelines
* enforcement of appropriate sanctions for failure to comply with HIPAA regulations
* development, implementation, and documentation of security awareness training.

=== Business Associate

A Business Associate is an entity that does not meet the definition of a covered entity but who performs, on behalf of a covered entity, a function or activity that is regulated by the HIPAA rules, including the Privacy Rule.

A HIPAA-regulated function or activity may involve the use or disclosure of PHI, or the provision of certain services to a covered entity that involves the use or disclosure of PHI.
The arrangement may be formal or informal.

=== Business Associate Agreement

HIPAA requires that a covered entity inform a Business Associate of the covered entity's designation as such and the requirement that the business associate must adopt and implement established standards and procedures for handling PHI.
Additionally, a Business Associate must be notified of its requirement to comply with all applicable provisions of the Privacy Rule.

Before a covered entity discloses PHI to a Business Associate, the covered entity must obtain assurances, generally in the form of a contract, that the Business Associate will appropriately safeguard the information.
A contract cannot authorize a business associate to use or disclose PHI in a manner that, if done by the covered entity, would violate the Privacy Rule.

=== Training

The appointing authority must ensure and document that all Division of Family Children Services (DFCS) employees complete HIPAA training as part of new employee orientation.

=== Penalties for Non-Compliance

HIPAA establishes both civil and criminal penalties for a covered entity that misuses PHI.

Civil violations of HIPAA standards can result in monetary penalties of up to $100 per violation, with a cap of $25,000 in a calendar year for all violations of the same requirement.

Criminal violations of HIPAA standards can result in penalties ranging from fines up to $50,000 and one year in prison for certain offenses, fines up to $100,000 and up to five years in prison for offenses committed under “false pretenses,” and fines up to $250,000 and up to 10 years in prison if the offenses are committed with the intent to use PHI for commercial purposes or if intending malicious harm.

=== Additional Information

Additional HIPAA information is available at https://www.hhs.gov/ocr/hipaa.
