= 1317 Continued Absence from The Home
:chapter-number: 1300
:effective-date: November 2019
:mt: MT-50
:policy-number: 1317
:policy-title: Continued Absence from The Home
:previous-policy-number: MT 23

include::partial$policy-header.adoc[]

== Requirements

Deprivation of parental support or care exists when one or both parents do not live in the home with the child and the absence interrupts the ability to function as a parent.

== Basic Considerations

The absence of the parent from the home must be established to determine if the child is deprived based on continued absence.

In order to establish deprivation, the following information must be determined:

* The parent is not living in the home.
* The absence of the parent interrupts the parent's ability to function as a parent.
* The absence of the absent parent (AP) is of an indefinite duration.

The following information about each AP must be obtained from the AU:

* name of the AP,
* AP's social security number,
* AP's date of birth,
* date of the last contact with the AP,
* address or phone number of a family member or friend who knows how to contact the AP,
* date and amount of the last child support received from the AP.

=== Absence from the Home

Deprivation is assumed to exist if one or both parents do not live in the home.

=== Incarceration

Deprivation exists if a parent is incarcerated.
The following information about the incarcerated parent must be established:

* crime for which the parent is incarcerated,
* location of the incarceration,
* length of sentence,
* minimum confinement,
* earliest parole date.

=== Adoption

Deprivation exists if a child is adopted by a single parent.

=== Joint Custody

Deprivation exists if the parents have joint custody of the child.

The following conditions must exist to establish deprivation in a joint custody situation:

* The time the child spends with the other parent is considered a visit.
* The time the child spends with the other parent may not exceed two months, or the child cannot be included in the AU for that period.
* The child may be eligible to receive cash assistance in either home but cannot be included in more than one AU in a month.

=== Absence Because of Court-Ordered Work

Deprivation because of continued absence exists when both parents are in the home, but one parent is performing court-ordered work under the following conditions:

* The parent has been convicted of an offense and is under sentence by a court.
* The sentence requires, and the parent is performing, unpaid public work or community service during the workday.
* The parent is permitted by the court to live at home while serving the sentence.

The following conditions apply to the parent in this situation:

* The parent is not included in the AU.
* The parent does not have a work requirement.
* The parent is not referred to Division of Child Support Services (DCSS).
* The parent's income and resources are considered available to the AU.
Refer to Section 1635, Allocation to an Ineligible Spouse.

The following information must be established regarding the court-ordered work:

* the conviction and sentence,
* the court's permission for the parent to live at home,
* the performance of unpaid community work.

=== Parental Functioning

Deprivation may not exist if a parent is absent from the home, but parental functioning is not interrupted.

Parental functioning includes maintenance, physical care and guidance on a day-to-day basis equal to the amount that would be provided if the parent were in the home.

Deprivation may be established, and interruption of parental functioning is assumed even though the AP is in contact with the child because of court-ordered visitation, joint custody, or informal agreement by the parents.

The AU can rebut the assumption of deprivation if parental functioning is not interrupted and the AP provides maintenance, physical care and guidance on a daily basis.

=== Temporary Absence

Deprivation may not exist if a parent is absent from the home on a temporary basis.
Temporary absences that do not constitute continued absence include the following:

* seeking employment
* military duty
* employment-related travel
* vacations.

Deprivation does not exist if the parent is on active duty in the uniformed services of the United States.
Uniformed services include the following:

* Air Force
* Army
* Coast Guard
* Marine Corps
* National Oceanographic and Atmospheric Administration
* Navy
* Public Health Service.

=== Contacting The AP

Direct contact with the AP must be attempted, unless there is good cause for not attempting contact.

_Refer to Section 1320, Child Support Enforcement._

The following resources are used to attempt to locate the AP if an address is unavailable:

* city directories
* STARS Interfaces
* Integrated Eligibility System (IES)
* other DFCS records
* telephone directories
* Internet browsing
* Clearinghouse DOL files.
+
NOTE: Clearinghouse files may be used only as a last resort and with supervisory approval.

_This list is not all-inclusive_.

A letter to the AP is sent when an address is available and there is no good cause claimed or established.
The letter to AP is sent:

* at application,
* when a new child is added to an AU, and the deprivation is based upon the continued absence of one or both parents from home,
* when a change in paternity is reported and the basis of deprivation is or continues to be the absence of one or both parents

The letter to AP is required for the following purposes:

* to verify deprivation if questionable,
* to verify payment of child support from the source,
* to notify the AP of the application for receipt of cash assistance for the child,
* to notify the AP of his or her debt to the state.

The AP letter is sent to the biological father if there is no legal father.

== Verification and Documentation

The AU's statement of the absence from the home is accepted unless questionable.

If questionable, the absence is verified through documentary evidence or third-party collateral.

== Procedures

=== Parent is Absent from Home

Follow the steps below when one or both parents are absent from the home.

[horizontal,labelwidth=10]
Step 1:: Assume that the absence from the home interrupts the AP's ability to function as a parent unless the AU rebuts the assumption.

Step 2:: Establish that the absence is of an indefinite duration.
Obtain required information about the AP.

Step 3:: Attempt contact with the AP by sending the absent parent letter to a known address.

Step 4:: If an address is unknown, attempt to locate the AP through other resources.

Step 5:: Document attempts to contact the AP.

Step 6:: Follow the steps below when an absent parent returns to the home.

=== AP Returns to Home

[horizontal,labelwidth=10]
Step 1:: Establish the date the AP returned to the home.

Step 2:: Determine if deprivation still exists based on the presence in the home of the other parent.

Step 3:: Determine if the AP may be incapacitated.

Step 4:: Determine if the child may now be deprived based on recent connection.
