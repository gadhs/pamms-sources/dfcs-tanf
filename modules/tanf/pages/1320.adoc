= 1320 Child Support Enforcement
:chapter-number: 1300
:effective-date: November 2023
:mt: MT-75
:policy-number: 1320
:policy-title: Child Support Enforcement
:previous-policy-number: MT 71

include::partial$policy-header.adoc[]

== Requirements

A caretaker, payee or minor parent must cooperate with the Division of Child Support Services (DCSS) for the assistance unit (AU) members to receive cash assistance, unless good cause exists.

== Basic Considerations

An absent parent (AP) whose child receives cash assistance has incurred a debt to the state because of the AP's failure to provide child support for his/her child.
This debt is incurred only when the AP is under a court order to pay child support during a month for which the AP's child receives cash assistance.

States are required to establish a program to enforce the AP's obligation to support his/her child.
DCSS administers this program in Georgia.

An AU's rights to child support are assigned by law to the state upon receipt of cash assistance.

DFCS performs the following functions:

* provides information about DCSS to the AU,
* obtains information about the AP for the DCSS referral,
* refers the AP to DCSS,
* determines if good cause for failure to cooperate with DCSS exists,
* takes appropriate action for failure to cooperate.

DCSS performs the following functions:

* locates the absent parent,
* establishes legal paternity,
* obtains child support court orders,
* collects, enforces, and distributes child support payments,
* notifies DFCS when the AU fails to cooperate,
* participates in hearings regarding good cause,
* reviews the good cause decisions made by DFCS.

=== Providing Information

The following information must be provided to the AU at initial application, at the standard review and when adding a child to the AU:

* an explanation of the child support program,
* an explanation of the assignment to the state of rights to child support,
* the requirement to cooperate with DCSS and the consequences for failing to cooperate,
* an explanation that the AU has the right to claim good cause at any time, the evidence needed to establish good cause, and the time frame for providing the evidence.

Form 138, Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in DCSS and Third Party Resource Requirements, must be discussed with the AU and/or the minor parent at application, review (if a change is reported), and when adding a dependent child to the AU.

=== Obtaining Information

The following information, if available, must be obtained from the AU:

* full name of AP,
* AP's home and business addresses,
* AP's social security number,
* AP's date of birth,
* date of last contact with the AP,
* address of a family member or friend who knows how to contact the AP,
* date and amount of any child support received from the AP.

NOTE: Refer to Section 1315, Deprivation, for policy on the requirement to name the AP as a part of the eligibility determination process.

=== Referrals

The AP of the dependent child included in the AU must be referred to DCSS.
The following individuals are not referred to DCSS.

* AP of a minor caretaker (head of household),
* AP of a married minor,
* AP of an 18-year-old dependent child

=== Cooperation with DCSS

An AU must cooperate with DCSS in locating the AP of a dependent child included in the AU, establishing the legal paternity of that parent, and obtaining child support from him/her, unless good cause exists.

Cooperation with DCSS includes but is not limited to the following:

* attending scheduled appointments with DCSS,
* providing the following information to DCSS about the AP:
** current or former address
** date of birth
** social security number
** other information that would assist DCSS in locating the AP, establishing paternity and/or obtaining child support from the AP,
* attesting to this information or attesting to lack of this information under penalty of law,
* submitting to a paternity test if paternity is questioned or denied.

When an AU fails to cooperate with DCSS, DCSS will notify DFCS and DFCS must determine if good cause exists.

=== Good Cause

Good cause may be claimed at any time during the application process or following approval.

Assistance is not delayed, denied, or terminated pending a determination of good cause if the AU has cooperated in providing information and supporting evidence.

Good cause can be established if one of the following circumstances exists:

* cooperation with DCSS may result in physical or emotional harm to the child, the grantee relative or the minor parent,
* the child was conceived as the result of rape or incest,
* legal proceedings for the adoption of the child are pending before a court,
* the parent is being assisted by a public or licensed social service agency to resolve the issue of whether to keep the child or release him/her for adoption and the discussions have not pended for more than three months.

The AU has primary responsibility for providing information and any required third-party evidence needed to establish good cause.
The agency must assist the AU in obtaining information and/or evidence upon request.

When third party evidence is required to establish good cause, it must be provided by the AU within 20 calendar days of claiming good cause.

Refer to <<chart-1320-1,Chart 1320.1>> in this section for evidence needed to establish good cause.

NOTE: This time frame can be extended at the request of the AU.
The reason must be documented.

When good cause is asserted on an active case, DCSS will suspend activity until a good cause determination is made.

A determination of whether good cause exists must be made within 45 calendar days of the application or 30 calendar days at any other time.

The following chart is used to determine the proof needed to substantiate a claim of good cause.

.Chart 1320.1 - Evidence Needed to Substantiate Good Cause Claim
[#chart-1320-1]
|===
^| GOOD CAUSE CIRCUMSTANCE ^| EVIDENCE REQUIRED

| Actual physical and/or emotional harm has been done to the child, or the potential for harm exists.
| AU's statement, unless questionable.
If questionable, third-party evidence must be obtained from one of the following sources:

Child Protective Services (CPS), court, criminal, law enforcement, medical, psychological or social services records indicating the possibility of physical or emotional harm by the AP.

| Actual physical and/or emotional harm has been done to the grantee relative, or the potential for harm exists.
| AU's statement, unless questionable.
If questionable, third-party evidence must be obtained from one of the following sources: court, criminal, law enforcement, medical, psychological or social services records indicating the possibility of physical or emotional harm by the AP.

| The child was conceived as a result of rape or incest.
| medical or law enforcement records indicating that conception resulted from rape or incest.

| Any of the above good cause circumstances mentioned above exist.
| written statements from individual(s) with knowledge of good cause circumstances when the above evidence cannot be obtained.

| Legal adoption proceedings are pending.
| court documents or a written statement of a social services worker indicating that an adoption is pending in court.

| A public or private social service agency is assisting the parent in deciding whether to keep the child or release the child for adoption.
| a statement from the public or private social service agency assisting the parent.
|===

=== Good Cause is Established

The AU must be notified of the decision regarding the good cause determination within two working days of the date the decision is made.
DCSS will not attempt to establish paternity or collect child support when good cause is established.

=== Good Cause is Not Established

When it is determined that good cause does not exist or it cannot be established, the AU must be allowed ten calendar days to choose one of the following options:

* cooperate with DCSS,
* request closure of cash assistance,
* request removal of the AP's child from the AU if that child is a non-SFU member,
* request a hearing,
* withdraw the application.

If a hearing is requested, the name and address of the local DCSS agent must be included on the hearing request.
The referral to DCSS should not be made nor is any action taken pending the hearing decision.

=== Failure to Cooperate with DCSS

If an AU member or the grantee relative fails to cooperate with DCSS without good cause, the AU is ineligible for cash assistance.

[caption=Exception]
NOTE: If the child is a non-SFU member, the AU has the option of requesting removal from the AU of the child of the AP about whom the individual fails to cooperate.

=== Reapplication

An AU that reapplies for cash assistance following termination of cash assistance because of non-cooperation with DCSS, must cooperate with DCSS prior to approval of the new application.

If the reason for the termination of cash assistance was other than non-cooperation with DCSS, but a notice of non-cooperation with DCSS was also issued prior to termination of cash assistance, then the AU must cooperate with DCSS prior to approval of the new application.

If the notice of non-cooperation was issued during a period of non-receipt of cash assistance, then cooperation with DCSS is required prior to approval of the new application.

Form 5706, TANF Child Support Services Compliance Agreement can be used to verify DCSS compliance, but it is not required.
If used, the individual must contact DCSS within 10 calendar days of signing the agreement, and if necessary, attend scheduled appointments.

DCSS gives priority to these cases and will notify DFCS whether the applicant cooperated.

If the AU has cooperated, the application must be approved if the AU is otherwise eligible.
If the AU has failed to cooperate, the agency determines whether good cause exists.

If good cause is established, the application is approved if the AU is otherwise eligible.
Notify DCSS immediately so it can close its case for the AP.

If good cause cannot be established, the AU must be allowed to choose from the same options listed under “Good Cause is Not Established” in this section.

== Procedures

=== Providing Information

Provide all required information to the AU at initial application, at review (if a change in paternity is reported) and when adding a child to the AU.

Review with the AU Form 138, Notice of Requirement to Cooperate and Right to Claim Good Cause for Refusal to Cooperate in DCSS and Third-Party Resource Requirements.
Provide the AU with a copy of this form.

=== Obtaining Information

Obtain the required information about the AP from the AU at initial application, at review (if a change in paternity is reported), and when adding a child to the AU.

Send Form 130, TANF and Family Medicaid - Child and Medical Support Letter, to AP if an address is available.

If Form 130 is returned by the AP, forward a copy to DCSS.

Refer to Section 1317, Continued Absence, for resources to use to attempt to locate the AP if an address is unavailable.

=== Referrals

Make the necessary referrals.
Refer to <<chart-1320-2,Chart 1320.2>> for special circumstances.

The chart below is used to determine whether to refer the AP to DCSS when special circumstances exist.

.Chart 1320.2 - DCSS Referrals in Special Circumstances
[#chart-1320-2]
|===
^| IF ^| THEN

2+^| *CASE SITUATIONS*

| a child born in wedlock is alleged to be the child of a man other than the mother's legal husband,
| refer the biological father.
Notify DCSS that there is a legal father.

| it is uncertain which of several men may be the AP,
| refer the man most likely to be the father of the child.
Notify DCSS that paternity is questionable.

| the identity of the AP is unknown,
| enter “unknown” for the absent parent's name.

| a claim is made that the AP is incapacitated or disabled or receives SSI or RSDI,
| refer the AP.
Notify DCSS of the claim that the AP is incapacitated or disabled.

| the only child receives SSI,
| do not refer the AP.

| the parent is living in the home while sentenced to perform unpaid court-ordered work,
| do not refer the AP.

| parental rights have been terminated by judicial determination.
| do not refer the AP.

| parental rights have been voluntarily relinquished,
| refer the AP.
|===

=== Determining Good Cause

Follow the steps below when an AU claims good cause:

[horizontal,labelwidth=10]
Step 1:: Notify the AU of the evidence needed to establish good cause and the time frame to provide the evidence.

Step 2:: Document the reason if this time frame is extended.
+
Refer to <<chart-1320-1,Chart 1320.1>> for types of documentary evidence needed to establish a good cause claim.

Step 3:: Notify DCSS immediately when an AU's claim of good cause involves an AP who has been previously referred so that DCSS may suspend activities pending the good cause determination.

Step 4:: Review the information provided by the AU and the available evidence.
+
Request additional evidence if necessary.

Step 5:: Assist in obtaining needed information if requested.
+
If the AU requests assistance in obtaining information, do not contact the AP unless necessary to determine good cause.
Notify the AU before contacting the AP.

Step 6:: Make a determination of good cause based on the available evidence.
+
Determine good cause within 45 calendar days of the application or within 30 calendar days at any other time.

Step 7:: Document the good cause determination.

=== Good Cause is Established

Notify the AU within two working days that good cause has been established and that DCSS will not pursue child support activities.

Review good cause circumstances at the next review, or more frequently if good cause is subject to change.

=== Good Cause is Not Established

Notify the AU within two working days of the decision that good cause cannot be established.
Allow the AU ten calendar days to choose one of the options available based on the denial of the good cause claim.

Take appropriate action based on the decision.

Do not act or refer to DCSS if a hearing is requested.

=== Notice of Noncooperation

Follow the steps below when DCSS notifies the agency that the AU has failed to cooperate:

[horizontal,labelwidth=10]
Step 1:: Discuss the allegations with the AU.

Step 2:: Discuss mitigating circumstances with the DCSS agent.

Step 3:: Determine if good cause exists.

Step 4:: If good cause exists, take appropriate action.

Step 5:: If good cause is not established, refer to previous procedures.

Step 6:: Notify DCSS of action taken.

=== TANF Reapplication Following Termination Due to Noncooperation

Follow the steps below when an individual reapplies for cash assistance following termination due to noncooperation:

[horizontal,labelwidth=10]
Step 1:: Inform the AU of the requirement to cooperate with DCSS prior to approval of cash assistance.

Step 2:: Mail Form 5706, TANF Child Support Services Compliance Agreement.
Remind the client the form must be signed.

Step 3:: Inform the applicant that s/he must contact DCSS and, if deemed necessary by DCSS, attend scheduled appointments.
+
NOTE: DCSS will give priority to clients who reapply for cash assistance after failing to cooperate with DCSS.

Step 4:: If DCSS provides notification that the AU has cooperated, approve the application if the AU is otherwise eligible.

Step 5:: If DCSS provides notification that the AU has failed to cooperate, determine whether good cause for non-cooperation exists.

Step 6:: If it is determined that good cause exists, approve the application if the AU is otherwise eligible.

Step 7:: If it is determined that good cause for noncooperation does not exist, deny the application for those individuals affected by the non-cooperation.
