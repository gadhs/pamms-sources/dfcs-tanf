= 1360 Immunization
:chapter-number: 1300
:effective-date: October 2024
:mt: MT-78
:policy-number: 1360
:policy-title: Immunization
:previous-policy-number: MT 56

include::partial$policy-header.adoc[]

== Requirements

A preschool age child must be properly immunized to be included in the assistance units (AU).

== Basic Considerations

A child under age seven who is not yet enrolled in public or private school must comply with TANF immunization requirements.

Public or private school includes kindergarten through grade 12.
This does not include preschool or pre-kindergarten.
Children enrolled in preschool or pre-kindergarten must meet TANF immunization requirements.

Enrollment in a public or private school or a registered home school is prima facie evidence that a child is properly immunized and therefore not subject to immunization requirements.
These children must meet other state immunization requirements that are monitored by the school.

The AU's statement that a child is enrolled in public or private school or a registered home school is acceptable, unless the agency has information that conflicts with the statement.

=== Required Immunizations

The immunizations must be consistent with the series required by Department of Human Services (DHS) that has been determined in collaboration with Department of Public Health and approved by the federal government for entry into a public or private school.

For Georgia the required immunization series begins at birth and continues through ages four to six years.

The preschool age child must begin the immunization series to be included in the AU and must continue the series to remain eligible.
To determine eligibility of a newborn child, refer to: <<Verification Procedures>>.

A child is considered current in the immunization series as long as the next immunization due date included on acceptable verification is a future date.

=== Penalty

A child who does not meet the immunization requirement and who is a member of the standard filing unit (SFU) is penalized until the requirement is met or good cause is established.

Refer to Section xref:1670.adoc[].

An individual who is not a member of the SFU is not penalized but is excluded from the AU.
Refer to Section xref:1205.adoc[], for additional information.

If the only child is penalized or excluded, the AU is not eligible to receive cash assistance.

=== Good Cause

The AU may claim good cause for failure or refusal to have the child immunized.
The following are acceptable reasons for establishing good cause:

* The child has a physical condition/disability that makes immunization undesirable.
The AU must provide verification to that effect from the health care provider.
The statement from the provider should include an estimate of the disability's expected duration.
+
NOTE: The child is exempt from the immunization requirement until this disability is no longer a factor.

* The parent or legal guardian of the child objects to immunization for religious reasons.
The AU must provide a statement or affidavit affirming that fact.
This statement does not have to be notarized.
+
NOTE: The religious objection of another relative is not an acceptable reason for good cause.
A non-parent grantee relative is not afforded good cause unless they have legal guardianship.
For example, grandparent or aunt/uncle could not claim good cause due to religious reasons unless they also have guardianship.

* The AU is unable to have the child immunized within the normal time-period for providing verification because of scheduling problems with the health care provider.
The AU's statement that an appointment has been scheduled is accepted, unless questionable.

* The AU must obtain verification from an out-of-state provider.
The AU's statement that the verification has been requested is accepted, unless questionable.

_This list is not all-inclusive._

Good cause is granted on a case-by-case basis.
Follow-up is required and must be scheduled as appropriate for each good cause situation.

== Verification

The AU must provide verification that a preschool age child began the immunization series and that the series continues.

The following are acceptable sources of verifications:

* An original, photocopy, or computer-generated version of DHS Form 3231, Certificate of Immunization.

* Child Care Immunization Certificate.

* Form 3231, Certificate of Immunization obtained using the web-based service of the Department of Public Health known as “Georgia Registry of Immunization Transactions and Services (GRITS)

* Alternate methods of verification are acceptable if they include the same information found on the Form 3231, Certificate of Immunization including a provider signature.

* Certificate of immunization issued by any health care provider who immunizes the child.
The verification must include a next immunization due date and the provider's signature.
This date must be a future date for the child to be considered current in the series.

* If the child was immunized in another state, a certification of immunization from the other state is acceptable, provided it contains the same information found on the Form 3231, Certificate of Immunization.

_This list is not all inclusive._

NOTE: If immunizations cannot be verified through an interface (i.e. GRITS) or a third-party source, client statement may be accepted as a last resort.
County DFCS offices may arrange with local providers to verify immunizations through direct contact.
This verification, however, must be signed by the provider and must include a next immunization due date.

Verification must be provided at application and in the month following the month in which the next immunization is due.

== Verification Procedures

=== Applications

Follow the steps below to verify immunizations for a preschool age child for whom a TANF application is filed:

[horizontal,labelwidth=10]
Step 1:: Explain the immunization requirements and discuss good cause.
+
Explain Health Check services available through Public Health and how the AU can obtain these services.

Step 2:: Require verification of immunization.
Instruct the AU to take the verification checklist to the provider.
+
--
[caption=Exception]
NOTE: If the child is under two months of age, add the child and request that immunization verification be provided the month following the month in which the child reaches age two months.

NOTE: If the AU will be taking the child to the county Health Department, provide a statement that the AU is a TANF applicant and is unable to pay for the immunizations.
--

Step 3:: Inform the AU that the child must continue the immunization series and that it will be verified the month after each immunization is due.

Step 4:: Document that the immunization requirement is met or that good cause is established, and that Health Check has been discussed.

Step 5:: Enter a task to contact the AU the month in which the next immunization is due.

Step 6:: Scan all documents into WebCenter Enterprise Capture (WEC).

=== Special Reviews

Follow the steps below to verify compliance with immunization requirements at special reviews:

[horizontal,labelwidth=10]
Step 1:: Contact the AU the month in which the immunization is due to notify him/her that the verification is due no later than the end of the next month.

Step 2:: Verify with supporting evidence that the immunization series is continuing.
Scan verification into WEC.
+
NOTE: If the verification is not obtained, follow the procedures for penalizing the child.

Step 3:: Document that the immunization requirement continues to be met and Health Check services were discussed.

=== Adding a Preschool Age Child to an AU

Follow the steps below to verify immunization requirements when adding a preschool age child to an AU:

[horizontal,labelwidth=10]
Step 1:: Explain the immunization requirements and discuss good cause.
+
Explain Health Check services available through Public Health and how the AU can obtain these services.

Step 2:: Require verification of immunization.
Instruct the AU to take the verification checklist to the provider.
+
--
[caption=Exception]
NOTE: If the child is under two months of age, add the child and require that immunization verification be provided the month following the month in which the child reaches age two months.

NOTE: If the AU will be taking the child to the County Health Department, provide a statement that the AU is a recipient and is unable to pay for the immunizations.
--

Step 3:: Inform the AU that the child must continue the immunization series and that it will be verified the month after each immunization is due.

Step 4:: Add the child to the AU when the verification is received.
Refer to Section xref:1740.adoc[].
