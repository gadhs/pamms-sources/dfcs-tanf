= 1380 Cooperation with Eligibility Investigations
:chapter-number: 1300
:effective-date: October 2024
:mt: MT-78
:policy-number: 1380
:policy-title: Cooperation with Eligibility Investigations
:previous-policy-number: MT 51

include::partial$policy-header.adoc[]

== Requirements

An AU must cooperate with an investigation or review conducted by one of the following:

* Office of Investigative Services (OIS)
* Fraud Prevention Investigator (FPI)
* Evaluation and Reporting (E&R)

== Basic Considerations

=== Penalty

Cooperation with an OIS or FPI investigation or an E&R review includes, but is not limited to, any or all of the following activities:

* appearing for an interview
* responding to questions
* providing requested documents
* providing any information deemed necessary by the investigator or reviewer.

If an AU does not cooperate with a SNAP (Supplemental Nutrition Assistance Program) investigation or review, the cash assistance is terminated when the information needed for the SNAP investigation or review could affect eligibility for cash assistance.

An AU must provide information and verification necessary to establish eligibility for historical months.

The entity conducting the investigation or review provides notification in writing to the worker if an AU has failed to cooperate.

Failure or refusal to cooperate with an eligibility investigation or review results in ineligibility of the AU.

If the AU agrees to cooperate during the timely notice period, closure of the case is delayed until E&R, OIS or the FPI notifies DFCS that the investigation has been completed.

If an AU reapplies for benefits after closure due to failure or refusal to cooperate with an eligibility investigation or review, all factors of eligibility must be verified by a third party prior to approval.

The following chart provides the action taken when information is requested for a historical month.

==== Chart 1380.1 - Failure to Cooperate in Providing Historical Information
[#chart-1380-1]
|===
^| IF ^| THEN

| the AU fails to provide information for a historical month
| each historical month for which requested information is not provided is an ineligible month.
Cash assistance is recouped for the appropriate historical months.

| the AU provides the information at a later date, after a claim has been scheduled
| eligibility is not redetermined for the historical month.
The claim amount remains valid.

| the AU provides the information at a later date, before a claim has been scheduled
| eligibility is redetermined for the historical month.
The claim amount is invalidated.

| the AU fails to provide information for a historical month that is needed to determine ongoing eligibility
| cash assistance is terminated, or the application is denied.
|===

== Procedures

Provide the AU with timely notice that the case will be closed if the worker receives notification from OIS, E&R or an FPI that an AU has failed or refused to cooperate.

Terminate the case following expiration of timely notice.

Verify all factors of eligibility prior to approval if an AU reapplies for cash assistance after denial or termination for failure to cooperate with an eligibility investigation.
