= 1635 Allocation to an Ineligible Spouse and/or Child
:chapter-number: 1600
:effective-date: May 2023
:mt: MT-72
:policy-number: 1635
:policy-title: Allocation to an Ineligible Spouse and/or Child
:previous-policy-number: MT 70

include::partial$policy-header.adoc[]

== Requirements

A budget is completed to determine the amount of an assistance unit (AU) member's income that can be allocated to meet the needs of his/her ineligible child and/or ineligible spouse.

== Basic Considerations

The amount of income allocated to meet the needs of an ineligible child and/or spouse is subtracted from the AU member's income to determine financial eligibility and benefit amount.

The allocated income is subtracted from the AU member's income as follows:

* prior to including the AU member's income in the gross income ceiling (GIC) test,
* prior to including the AU member's income in the standard of need (SON) trial budget,
* after applying the earned income deductions to the AU member's income in the budget which determines the benefit amount.

The allocated income may be a different amount in each step of the budgeting process.

Income may be allocated to the spouse or child of an AU member or to the spouse or child of a penalized or disqualified individual when the spouse or child is not eligible to be included in the AU.

Income is allocated as follows:

* using only the income of the spouse for the support of his/her ineligible spouse,
* using only the income of the parent for the support of his/her ineligible child.

Income may be allocated to the following individuals:

* Ineligible spouse of an eligible adult or child
* Ineligible parent of a pregnant minor head of household or minor parent who is the head of household
* Ineligible spouse of the non-parent specified caretaker who is receiving TANF cash assistance for themselves.

Income is not allocated to a spouse or child who is not included in the AU for one of the following reasons:

* voluntarily excluded
* penalized – count all income in budget
* receiving SSI
* disqualified – count all income in budget
* a child ineligible because of age

== Procedures

Follow the steps below to determine the amount of an individual's income that is allocated to an ineligible spouse and/or child in each step of the budgeting process:

[horizontal,labelwidth=10]
Step 1:: Determine the number of individuals to whom income can be allocated.
(This includes an unborn child)

Step 2:: Determine the gross countable earned and unearned income of the individual from whom income can be allocated.

Step 3:: Allocate by subtracting the SON for the number of individuals determined in Step 1 from the gross countable income determined in Step 2.
+
--
If a deficit exists, do not include any of the income of the individual from whom income is allocated in the AU's GIC test.

If a surplus exists, include the surplus in the GIC test.
--

Step 4:: Determine the gross countable earned and unearned income of other AU members and add this amount to the surplus, if any, determined in Step 3.

Step 5:: Complete the GIC test by comparing the AU's income determined in Step 4 to the GIC for the AU size.
+
--
If the AU's income is greater than the GIC, deny or terminate cash assistance.

If the AU's income is equal to or less than the GIC, there is earned income in the budget, and this is an application, proceed to Step 6.

If the AU's income is equal to or less than the GIC and there is no earned income or this is not an application, a SON trial budget is not required.
Proceed to Step 11.
--

Step 6:: Begin the SON trial budget by subtracting the $250 standard work deduction and dependent care expenses from the gross countable earned income of the individual from whom income can be allocated to determine his/her adjusted gross countable earned income.

Step 7:: Add the unearned income of the individual to the adjusted gross countable earned income determined in Step 6 to determine the individual's adjusted gross countable income.

Step 8:: Subtract the SON for the number of individuals to whom income can be allocated from the adjusted gross countable income determined in Step 7.
+
--
If a deficit exists, do not include any of the income of the individual from whom income is allocated in the AU's SON trial budget.

If a surplus exists, include the surplus in the AU's SON trial budget.
--

Step 9:: Add the adjusted gross countable income of other AU members to the surplus, if any, from Step 8.

Step 10:: Complete the SON trial budget by comparing the income from Step 9 to the SON for the AU size.
+
--
If this income is greater than or equal to the SON for the AU size, deny or terminate cash assistance.

If this income is less than the SON for the AU size, the AU passes the SON trial budget.
Proceed to Step 11 to determine the benefit amount.
--

Step 11:: Subtract the appropriate earned income deductions from the gross countable earned income of the individual from whom income can be allocated as determined in Step 1 to determine his/her net countable earned income.

Step 12:: Add the countable unearned income from Step 1 to the income from Step 11.
The result is the amount of income available for allocation in the budget that determines the benefit amount.

Step 13:: Allocate to the ineligible spouse and/or child the appropriate SON or the amount from Step 12, whichever is less.

Step 14:: Add the amount remaining from Step 13 to the net countable income of other AU members.

Step 15:: Subtract the net countable income of the AU from the SON for the AU size.

Step 16:: Authorize cash assistance in the amount of the deficit or the family maximum for the AU size, whichever is less.
