= 1665 Budgeting the Income of a Disqualified Individual
:chapter-number: 1600
:effective-date: January 2020
:mt: MT-52
:policy-number: 1665
:policy-title: Budgeting the Income of a Disqualified Individual
:previous-policy-number: MT 37

include::partial$policy-header.adoc[]

== Requirements

A budget is completed to determine the financial eligibility and benefit amount of the assistance unit (AU) when a disqualified individual's income is included.

== Basic Considerations

An individual is disqualified from receiving cash assistance when s/he has committed an intentional program violation (IPV).

Refer to Section 1385, Intentional Program Violations.

The countable income of the disqualified individual is included in the budget if the individual is a member of the standard filing unit (SFU).

The income of the disqualified individual who is not a member of the SFU is included only if the individual was included in the AU at the time the IPV was committed.

The needs of the disqualified individual are not considered when determining the gross income ceiling (GIC), the standard of need (SON) and the family maximum to use in the budgeting process.

Earned income deductions may be applied to the earned income of the disqualified individual.

Refer to Section 1615, Deductions, for information on determining eligibility for earned income deductions.

== Procedures

Follow the steps below to determine financial eligibility and benefit amount when a disqualified individual's income is included in the budget:

[horizontal,labelwidth=10]
Step 1:: Determine the gross countable income of the disqualified individual.

Step 2:: Add the income determined in Step 1 to the gross countable income of the AU members.

Step 3:: Complete the GIC test by comparing the income determined in Step 2 to the GIC for the AU size, excluding the needs of the disqualified individual.
+
--
If the gross countable income is greater than the GIC, deny or terminate cash assistance.

If the gross countable income is equal to or less than the GIC, proceed to Step 4.
--

Step 4:: Complete a SON trial budget, if necessary, to determine eligibility of an AU member and/or the disqualified individual for the $250 standard work deduction.
If not necessary, proceed to Step 5.
+
--
Do not include the needs of the disqualified individual when determining the SON to use in the trial budget.

If ineligible in the SON trial budget, deny or terminate cash assistance.
If eligible in the SON trial budget, proceed to Step 5.
--

Step 5:: Apply applicable earned income deductions to the gross countable earned income of each employed AU member and the earned income of the disqualified individual to determine net earned income.

Step 6:: Add the unearned income of AU members and the disqualified individual to the net earned income determined in Step 5 to determine net countable income.

Step 7:: Subtract the net countable income determined in Step 6 from the SON for the AU size, excluding the needs of the disqualified individual.
+
--
If the net countable income is greater than or equal to the SON, deny or terminate cash assistance.

If net countable income is less than the SON, proceed to Step 8.
--

Step 8:: Compare the deficit from Step 7 to the family maximum for the AU size, excluding the needs of the disqualified individual.

Step 9:: Authorize cash assistance in the amount of the deficit or the family maximum, whichever is less.
