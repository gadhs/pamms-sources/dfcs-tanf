= 1835 Failure to Participate
:chapter-number: 1800
:effective-date: January 2020
:mt: MT-53
:policy-number: 1835
:policy-title: Failure to Participate
:previous-policy-number: MT 33

include::partial$policy-header.adoc[]

== Requirements

Refusal to participate in the development of the TANF Family Service Plan (TFSP) or to sign it, or refusal / failure to meet the requirements of the TFSP without good cause results in the imposition of a sanction against the AU.

== Basic Considerations

A sanction may be imposed against an AU when a recipient with a personal responsibility or work requirement fails, without good cause, to meet agreed upon personal responsibility or work requirement.

Failure to meet a personal responsibility or work requirement without good cause constitutes a material violation.

A recipient may be participating in more than one activity at the same time, and a failure to meet the participation requirements in any one of the activities may result in the imposition of a sanction.

When a material violation has been committed, or if it is suspected that a material violation has been committed, the conciliation process must be followed so that a sanction may be avoided.

The conciliation process is available to TANF applicants and recipients only one time during the TANF life-time limit of 48-months.

Sanctions are imposed in a progressive two-step reduction/termination process.
An AU will receive a 25% reduction in cash assistance for the first material violation without good cause, and a termination of cash assistance for a second material violation without good cause.
If the AU reapplies after the initial termination period expires, the two-step process begins again with subsequent sanctions, but with a longer termination period.

There is no compliance process once a sanction has been imposed.

Refer to xref:1351.adoc[] for complete information about the sanction process.

=== Participation Requirements

The recipient must comply with the following participation requirements:

* participate at a level equal to full-time (40 hours per week) employment when it is determined by the agency to be possible;
* maintain employment,
* meet applicant job search requirements,
* complete the assessment and participate in the development of a TFSP,
* meet the satisfactory progress and hours per week requirements for assigned work activities on TANF Family Service Plan Work Plan - (Form 196A),
* fulfill the personal responsibilities as stated on the TANF Family Service Plan - (Form 196),
* cooperate in verifying attendance and provide documentation of progress in activities,
* cooperate with the case manager, training providers, and other appropriate staff,
* keep appointments, and
* respond to appropriate job referrals and accept bona fide job offers.

This list is not all-inclusive.

=== Types of Refusal to Participate

An overt refusal is a written or oral statement by the client that s/he will not participate.

A de facto refusal is an action or pattern of behavior from which failure to participate can be inferred.
Examples include but are not limited to the following:

* failing to keep appointments without good cause,
* refusing to report to an assigned activity,
* refusing to report for a job interview,
* refusing to accept a legitimate job offer, which pays equal to or more than minimum wage.
* refusing to accept suitable childcare, transportation or other support services,
* causing serious disruption at a work activity site,
* failing to meet hours per week requirements for assigned activities,
* quitting a work activity without prior consultation with the case manager,
* quitting a job without good cause.

=== Voluntary Quit

For applicant and recipient who voluntarily and without good cause quits a job which involves:

* 30 hours or more per week *or*
* weekly earnings equivalent to the federal minimum wage multiplied by 30 hours *and*
* voluntarily quit is within 30 days of the date of application or anytime thereafter.

NOTE: Terminating a self-employment enterprise or resigning from a job at the request of the employer shall not be considered voluntary quit.

A notice of the minimum sanction period must be sent on all voluntary quit or voluntary reduction decisions.

NOTE: If the case remains active, the worker must enter a task to contact the AR to determine eligibility at the end of the minimum sanction period.

A determination of good cause is made prior to applying a sanction.
Prior to applying a sanction, contact is made with the AU to determine if good cause exists.

An explanation of good cause policy is provided to the work eligible adult at the orientation/assessment appointment and documented on the work plan.

Refer to xref:1351.adoc[] for information on sanctions and good cause.

Sanctions are not imposed if the case manager determines that the client has good cause for failing to participate in a required activity.
If necessary, the case manager refers the recipient for resources that can help to overcome those barriers that prevent the client from fulfilling participation requirements.
Good cause is temporary.

=== Good Cause

Examples of good cause include but are not limited to the following:

* an illness or medical condition which is obvious or otherwise substantiated,
* a subpoena to appear in court,
* the unavailability of childcare, transportation or other needed services,
* the unavailability of custodial care for an incapacitated family member who resides with the client (the case manager should consider whether community service is appropriate for the client's work activity),
* the occurrence of a natural disaster,
* a family or personal crisis, including domestic violence,
* declining a job offer that pays less than the minimum wage,
* declining or quitting the job or training program that presents a risk to the client's health or safety,
* a previous mandatory participant now meets exemption criteria and wants to exercise that option.

Refer to xref:1351.adoc[] for policies and procedures governing the determination of good cause, conciliation of a material violation, and the imposition of sanctions.

=== TANF Sanctions

=== Conciliation of Work Requirements

The following are applicable to the conciliation of a failure to meet requirements of work activities or of the TFSP:

* If the conciliation appointment is kept, and the participant has good cause for not meeting work requirements, the case manager must resolve with the participant the problems that prevent him/her from meeting those requirements.
+
After the resolution of the problems, the participant must be assisted to complete an updated TANF Family Service Plan (Form 196A) and return to work activities as quickly as possible.

* When good cause is established, the failure to meet work activity requirements is not a material violation, and there is no conciliation.
* If the conciliation appointment is kept, and the participant does not have a good cause for not meeting work requirements, but the matter is resolved, then the TFSP needs to be updated and the participant must return to an activity quickly.
It is the responsibility of the case manager to explain and remind the participant of the lifetime limit.
This is considered a conciliated material violation.
+
The case manager must inform the participant that a subsequent failure on the part of the participant to meet any TFSP requirement will result in the imposition of a sanction, with no opportunity to conciliate.

* If the conciliation appointment is kept, the participant does not have a good cause for not meeting work requirements, and the matter is not resolved, the case manager must take an action to impose a sanction.
Refer to xref:1351.adoc[].
* If the TANF eligibility worker is not the employment services case manager, the ES case manager must refer the case to the TANF worker to impose the sanction.
* If the sanction is a 25% reduction, the AU must be contacted in the third month of the reduction period to discuss and update the service plan and prepare the participant to start his/her activity at the beginning of the fourth month.
* If the conciliation appointment is not kept, the TANF case must be closed for failure to keep an appointment.
Conciliation will remain in pending status until, and if, the client reapplies.
+
If the conciliation appointment is kept and the participant currently meets the criteria for exemption from work requirements, she has never exercised the option to be exempt, and now wishes to exercise that option, the participant may be exempt.
However, in an event that the client chooses to participate in a work activity, the case manager must explain to the client TANF lifetime limit, the time left on TANF for the client, and availability of Support Services, if needed to gain or maintain a job.
