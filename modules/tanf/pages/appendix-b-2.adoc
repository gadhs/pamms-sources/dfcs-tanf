= Appendix B Fair Hearing Responsibilities
:chapter-number: Appendix B
:effective-date: February 2022
:mt: MT-65
:policy-number: N/A
:policy-title: Fair Hearing Responsibilities
:previous-policy-number: MT 61

include::partial$policy-header.adoc[]

== Requirements

OSAH has specific duties regarding the requirements for and conduct of a hearing, which are consistent with Georgia's Administrative Procedure Act, other applicable laws, regulations, and OSAH's Administrative Rules of Procedure found at 616-1-2-.01 _et seq._

=== Office of State Administrative Fair Hearings Responsibilities

OSAH initiates the following actions as needed:

* provides, at least ten days prior to the hearing, advance written notice to all involved parties in order to permit adequate preparation of the case.
* changes the time and/or location of the hearing upon its own motion or for good cause shown by the applicant, recipient, or protective payee.
* consolidating cases for which the sole issue is one of state and/or federal law, regulations, or policy.
* conducts a single hearing for multiple programs, if determined appropriate.
* determines numbers of persons who may attend the hearing.
* denies or dismisses a hearing request and grants continuances.
* utilizes only the facts and opinions that are evidence of record or which may be officially noticed and are, therefore, subject to the rights of objection, rebuttal, and/or cross examination by the involved parties.
* makes a decision within ninety days from the date of DFCS' receipt of the request for a fair hearing.
* mails the final hearing decision and related hearing notices to all involved parties.
* informs the Petitioner of his/her Superior Court appeal rights, if the Petitioner disagrees with the final decision.

Upon receipt of a hearing request, OSAH notifies the county DFCS office and the applicant or recipient of the date and time of the hearing.
The hearing may be conducted in the county at the DFCS or other government office, at the OSAH office in Atlanta or by telephone.

State law prohibits the ALJ from providing legal advice to either party, including the state agency.
As such, OSAH cannot assist the agency or the petitioning applicant or recipient in determining who should be present as witnesses at the hearing or what evidence is necessary to establish the case.

The basis for an applicant's or recipient's request for a fair hearing can include consideration of:

* an agency action (e.g., initial and subsequent eligibility determinations, amount of TANF cash assistance or change in work support payments);
* failure to act with reasonable promptness on a claim for financial assistance, which includes undue delay in reaching a decision on eligibility or in making a payment;
* refusal to consider a request for or undue delay in making an adjustment in payment;

The applicant, recipient, or the AU's representative have rights to examine the contents of the case record and documents, refer to Appendix B: Fair Hearings.

=== Rights and Responsibilities of Both Parties at the Fair Hearing

Refer to Section 1002, Confidentiality, for additional information, including what cannot be released and penalties for unauthorized release.

The applicant, recipient, or the AU's representative also has the right to do the following:

* present the case with or without the aid of a representative, including legal counsel, a relative, friend or other spokesperson.
* request assistance from the agency for transportation to/from the hearing.
* present arguments without undue interference.

The applicant, recipient, and the agency present its case by (list is not exhaustive):

* bringing and/or requesting the appearance of witnesses by subpoena (if needed),
* establishing all pertinent facts and circumstances,
* questioning and refuting any testimony or evidence, including the opportunity to question and cross-examine adverse witnesses.

DFCS is responsible for the following:

* ensure the presence at the hearing of staff members with direct knowledge of the facts in dispute (See the _Procedures for Providing Policy Support for Fair Hearings which are available on the TANF share-point site._),
* ensure that all relevant agency records and copies are legible and available as evidence, and
* ensure that non-agency witnesses and records are present, either voluntarily or by subpoena.

=== The Final Hearing Decision

The final hearing decision is issued within ninety days from the date the written request for a hearing is received by DFCS, except in the event of a postponement or continuance.
Hearing decisions are based on the evidence presented at the hearing.

An ALJ shall have all the powers of the ultimate decision maker in the agency with respect to a contested case.
Hearing decisions specify the reason for the decision, which includes findings of fact, conclusions of law, and a disposition of the case.

The following shall be a part of the hearing record:

[loweralpha]
. all rulings, orders, and notices issued by the court.
. all pleadings and motions.
. all recordings or transcripts of oral hearings or arguments.
. all written direct testimony.
. all other data, studies, reports, documentation, information, other written material of any kind, and physical evidence submitted in the proceedings.
. a statement of matters officially noticed.
. all proposed findings of fact, conclusions of law, and briefs; and
. the Decision issued in the matter

OSAH maintains the official fair hearing record that meets criteria for its procedures.

The following chart is used to determine how to adjust cash assistance based on the outcome of OSAH's final decision:

.Chart B.2 – Adjustment of Cash Assistance Based on the Final Decision from a Fair Hearing
[#chart-b-2]
|===
^| IF ^| THEN

| cash assistance was continued or reinstated prior to the final decision from the ALJ and the decision is favorable to the AU,
| cash assistance is continued.

| cash assistance was not continued or reinstated prior to the final decision from the ALJ and the decision is favorable to the AU,
| cash assistance is approved retroactively, and corrective payments are issued as directed by the ALJ.

| cash assistance was continued or reinstated prior to the final decision from the ALJ and the decision is favorable to the agency,
| the Final Decision must be acted upon within ten (10) days of the agency's receipt of the Final Decision.

Corrective payments (overpayments or underpayments) may need to be established based on the decision received.

| cash assistance was not continued or reinstated prior to the final decision from the ALJ and the decision is favorable to the agency.
| no changes are made.
|===
